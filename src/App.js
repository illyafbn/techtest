import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css';

//Import custom components:
import PetList from './components/pet/pet-list';
import Loader from './components/loader';
import ControlPanel from './components/control-panel';
import Pagination from './components/pagination';

//Helpers:
import { validURL } from './etc';

function App() {

  //I used hooks for the first time in this application.
  //A better option for me was to use redux but I decided to challange myself
  const [petsLoaded, setLoadPets] = useState(false);
  const [loading, setLoading] = useState(true);
  const [allPets, setAllPets] = useState([]);
  const [filteredPets, setFilteredPets] = useState([]);
  const [visiblePets, setVisiblePets] = useState([]);
  const [visiblePetsLength, setVisLength] = useState(10);
  const [canShowMorePets, setCanShowMorePets] = useState(true);
  const [currentFirstIndex, setCurrentFirstIndex] = useState(0);



  const getPets = () => {
    console.log("GETTING PETS");
    setLoadPets(true);
    setLoading(true);
    axios.get('https://petstore.swagger.io/v2/pet/findByStatus?status=available&status=pending&status=sold').then((response) => {
      const _tempPetsAll = [];
      response.data.map((pet, index) => {
        //console.log(pet);
        let category = "";
        let image = "";
        if (pet.category == null)
          category = "unknown";
        else
          category = pet.category.name;
        if (pet.photoUrls == null)
          image = null;
        else {
          pet.photoUrls.map((url) => {
            if (validURL(url))
              image = url;
          });
        }
        const petObj = {
          id: pet.id,
          name: pet.name,
          category: category,
          image: image,
          selected: false,
          status: pet.status,
        };
        _tempPetsAll.push(petObj);
      });
      setAllPets(_tempPetsAll);
      setFilteredPets(_tempPetsAll);
      //Finish getting and storing the data.
      //Show only first 10:
      const _visiblePets = _tempPetsAll.slice(0, 10);
      setVisiblePets(_visiblePets);
      console.log(_tempPetsAll.length);
      console.log(_visiblePets.length);
      setLoading(false);
    });
  };

  const showMorePets = () => {
    if (visiblePetsLength < 40) {
      setVisiblePets(filteredPets.slice(currentFirstIndex, visiblePetsLength + 10));
      setVisLength(visiblePetsLength + 10);
    }
    else return;
  }

  const showLessPets = () => {
    if (visiblePetsLength > 10) {
      setVisiblePets(filteredPets.slice(currentFirstIndex, visiblePetsLength - 10));
      setVisLength(visiblePetsLength - 10);
    }
    else return;
  }

  const nextPage = () => {
    if (currentFirstIndex + visiblePetsLength < filteredPets.length) {
      const goToIndex = currentFirstIndex + visiblePetsLength;
      setCurrentFirstIndex(goToIndex);
      setVisiblePets(filteredPets.slice(goToIndex, goToIndex + visiblePetsLength));
      console.log(goToIndex);
    }
    else
      console.log("CANNOT NEXT PAGE");
  }

  const prevPage = () => {
    if (currentFirstIndex - visiblePetsLength >= 0) {
      console.log("PREV PAGE")
      const goToIndex = currentFirstIndex - visiblePetsLength;
      setCurrentFirstIndex(goToIndex);
      setVisiblePets(filteredPets.slice(goToIndex, goToIndex + visiblePetsLength));
      console.log(goToIndex);

    }
    else return;
  }

  const filterByType = (v) => {
    console.log(v.target.value);
    let x = v.target.value;
    setCurrentFirstIndex(0);
    if (x === "Please Select a Type") {
      setFilteredPets(allPets);
      setVisiblePets(allPets.slice(0, visiblePetsLength));
      return;
    } else if (x === "Other") {
      const returnedArray = allPets.filter(z => z.category != 'cats' && z.category != 'dogs');
      setFilteredPets(returnedArray);
      setVisiblePets(returnedArray.slice(0, visiblePetsLength));
      return;
    } else {
      x = x.charAt(0).toLowerCase() + x.slice(1) + "s";
      const returnedArray = allPets.filter(z => z.category == x);
      setFilteredPets(returnedArray);
      setVisiblePets(returnedArray.slice(0, visiblePetsLength));
      return;
    }
  };

  /*
  //Due to the time restrictions I was not able to fully implement this code due to some bugs.
  
  
    //Export Function:
    const setForExport = (index) => {
      const pet = allPets[index];
      console.log(pet);
    };
  
    const exportFile = () => {
      //Get the list of pets that are selected:
  
    };
  */


  //Hook effects:
  //This is used to get the data from the server once off.
  useEffect(() => {
    getPets();
  }, []);


  return (
    <div className="App">
      <div className="row mx-0">
        <div className="col"><h1 className="display-3">My Little Pet Shop</h1></div>
      </div>
      <div className="row mx-5 mt-5">
        <div className="card col px-0">
          {(petsLoaded) ? <div className="card-header mx-0"><ControlPanel nextPage={nextPage} prevPage={prevPage} filterByType={filterByType} showMoreCall={showMorePets} showLessCall={showLessPets} refresh={getPets} isPetListLoading={loading} /></div> : null}
          <div className="card-body">
            {
              /*(!petsLoaded)
                ? <button className="btn btn-primary my-5" onClick={getPets}>Load Available Pets</button>
                :*/ (loading)
                ? <Loader />
                : <PetList pets={visiblePets} />
            }
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
