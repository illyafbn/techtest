import React from 'react';
import Pagination from './../pagination';

export default (props) => <div className="row">
    <div className="col-sm-12 col-md-3">
        <div className="form-group text-left">
            <label>Type:</label>
            <select className="form-control" onChange={props.filterByType}>
                <option>Please Select a Type</option>
                <option>Dog</option>
                <option>Cat</option>
                <option>Other</option>
            </select>
        </div>
    </div>
    <div className="col-sm-12 col-md-4 my-auto">
        <Pagination nextPage={props.nextPage} prevPage={props.prevPage} />
    </div>
    <div className="col-sm-12 col-md-5 my-auto">
        <button className="btn btn-danger mr-2" onClick={props.showLessCall}><i className="fas fa-minus mx-1" /> Show Less</button>
        <button className="btn btn-primary mr-2" onClick={props.refresh} disabled={props.isPetListLoading}><i className="fas fa-sync-alt mx-1" /> Refresh</button>
        <button className="btn btn-success mr-2" onClick={props.showMoreCall}><i className="fas fa-plus mx-1" /> Show More</button>
        {/*I started on this code but due to some bugs I decided to leave it out in order to finish in time.*/}
        <button className="btn btn-info" onClick={props.export} disabled={true}><i className="fas fa-save" /> Export</button>
    </div>
</div>;