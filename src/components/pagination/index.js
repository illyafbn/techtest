import React from 'react';

export default (props) => {
    return <div className="row ">
        <div className="col-6"><button className='btn btn-danger' onClick={props.prevPage}>Previous Page</button></div>
        <div className="col-6"><button className='btn btn-success' onClick={props.nextPage}>Next Page</button></div>
    </div>;
};