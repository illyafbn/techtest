import React from 'react';

export default (props) => <div className="row my-5">
    <div className="col-12 my-auto">
        <i className="fas fa-fan fa-spin fa-7x d-block" />
    </div>
    <div className="col-12 mt-3">
        <p className="display-4">Loading...</p>
    </div>
</div>