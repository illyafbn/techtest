import React from 'react';

export default (props) => {
    const pet = props.pet;

    let category = "";
    let icon = "";
    switch (pet.category) {
        case "cats":
            category = "Cat";
            icon = "fas fa-cat fa-10x";
            break;
        case "dogs":
            category = "Dog";
            icon = "fas fa-dog fa-10x";
            break;
        default:
            category = "Unknown";
            icon = "fas fa-question-circle fa-10x";
            break;
    }

    const colors = [
        "#000000",
        "#0000FF",
        "#0095B6",
        "#8A2BE2",
        "#B87333",
        "#FFD700",
        "#DA70D6",
        "#003153",
        "#92000A",
        "#40826D"
    ];



    return <div className="card my-2">
        <div className="card-body">
            <div className="row">
                {/*<div className="col-sm-12 col-md-1">
                    <input type="checkbox" onChange={() => props.setForExport(props.index)} value={pet.selected} />
</div>*/}
                <div className="col-sm-12 col-md-4">
                    {
                        (pet.image == null || pet.image == "")
                            ? <i className={icon} style={{ color: colors[Math.floor(Math.random() * Math.floor(colors.length))] }} />
                            : <img className="col-8" src={pet.image} alt={category + ": " + pet.name} />
                    }
                </div>
                <div className="col-sm-12 col-md-8 text-left">
                    <h4><b>Name:</b> {pet.name}</h4>
                    <h6><b>Type:</b> {category}</h6>
                    <h6><b>Status:</b> {pet.status.charAt(0).toUpperCase() + pet.status.slice(1)}</h6>
                    <p><b>Animal ID:</b> {pet.id}</p>
                </div>
            </div>
        </div>
    </div>;
}