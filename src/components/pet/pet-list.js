import React from 'react';

import PetCard from './index';

export default (props) => <ul className="list-group">
    {props.pets.map((pet, index) => <PetCard key={index} pet={pet} index={index} />)}
</ul>